/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
require([
    'jquery',
    'IAGC_ProductLabel/js/label'
], function ($,label) {
    $('.columns').css('position','relative');
    var width = $(window).width();
    var height_img,width_img;
    $(document).on('gallery:loaded', function () {
        setTimeout(function () {
            height_img=$('.gallery-placeholder').height();
            width_img=$('.gallery-placeholder').width();
            if (width < 768){

                //Out Of Stock Label
                label.productOnMobile(".outofstock-label",pos1,height_img,$('.outofstock-label').height());
                //New Product Label
                label.productOnMobile(".newproduct-label",pos2,height_img,$('.newproduct-label').height());
                //Sale Product Label
                label.productOnMobile(".saleproduct-label",pos3,height_img,$('.saleproduct-label').height());

                //Sale Product Label
                label.productOnMobile(".skin-type-normal-label",pos4,height_img,$('.skin-type-normal-label').height());
                //Sale Product Label
                label.productOnMobile(".skin-type-dry-label",pos5,height_img,$('.skin-type-dry-label').height());
                //Sale Product Label
                label.productOnMobile(".skin-type-oily-label",pos6,height_img,$('.skin-type-oily-label').height());
                //Sale Product Label
                label.productOnMobile(".skin-type-combination-label",pos7,height_img,$('.skin-type-combination-label').height());
                //Sale Product Label
                label.productOnMobile(".skin-type-sensitive-label",pos8,height_img,$('.skin-type-sensitive-label').height());
                //Sale Product Label
                label.productOnMobile(".skin-type-aged-label",pos9,height_img,$('.skin-type-aged-label').height());
                //Sale Product Label
                label.productOnMobile(".skin-type-acne-prone-label",pos10,height_img,$('.skin-type-acne-prone-label').height());

            } else { 

                //Out Of Stock Label
                label.productOnWindow(".outofstock-label",pos1,width_img-$('.outofstock-label').width()-50,height_img,$('.outofstock-label').height()+20);
                //New Product Label
                label.productOnWindow(".newproduct-label",pos2,width_img-$('.newproduct-label').width()-50,height_img,$('.newproduct-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".saleproduct-label",pos3,width_img-$('.saleproduct-label').width()-50,height_img,$('.saleproduct-label').height()+20);

                //Sale Product Label
                label.productOnWindow(".skin-type-normal-label",pos4,width_img-$('.skin-type-normal-label').width()-50,height_img,$('.skin-type-normal-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".skin-type-dry-label",pos5,width_img-$('.skin-type-dry-label').width()-50,height_img,$('.skin-type-dry-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".skin-type-oily-label",pos6,width_img-$('.skin-type-oily-label').width()-50,height_img,$('.skin-type-oily-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".skin-type-combination-label",pos7,width_img-$('.skin-type-combination-label').width()-50,height_img,$('.skin-type-combination-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".skin-type-sensitive-label",pos8,width_img-$('.skin-type-sensitive-label').width()-50,height_img,$('.skin-type-sensitive-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".skin-type-aged-label",pos9,width_img-$('.skin-type-aged-label').width()-50,height_img,$('.skin-type-aged-label').height()+20);
                //Sale Product Label
                label.productOnWindow(".skin-type-acne-prone-label",pos10,width_img-$('.skin-type-acne-prone-label').width()-50,height_img,$('.skin-type-acne-prone-label').height()+20);
            }
        }, 3000);
    });

    var pos1=$('#outofstock-position').html();
    var pos2=$('#newproduct-position').html();
    var pos3=$('#saleproduct-position').html();

    var pos4=$('#skin-type-normal-position').html();
    var pos5=$('#skin-type-dry-position').html();
    var pos6=$('#skin-type-oily-position').html();
    var pos7=$('#skin-type-combination-position').html();
    var pos8=$('#skin-type-sensitive-position').html();
    var pos9=$('#skin-type-aged-position').html();
    var pos10=$('#skin-type-acne-prone-position').html();

    $(window).resize(function(){

        width = $(window).width();
        height_img=$('.gallery-placeholder').height();
        width_img=$('.gallery-placeholder').width();

        if (width < 768){
            //Out Of Stock Label
            label.productOnMobile(".outofstock-label",pos1,height_img,$('.outofstock-label').height());
            //New Product Label
            label.productOnMobile(".newproduct-label",pos2,height_img,$('.newproduct-label').height());
            //Sale Product Label
            label.productOnMobile(".saleproduct-label",pos3,height_img,$('.saleproduct-label').height());

            //Sale Product Label
            label.productOnMobile(".skin-type-normal-label",pos4,height_img,$('.skin-type-normal-label').height());
            //Sale Product Label
            label.productOnMobile(".skin-type-dry-label",pos5,height_img,$('.skin-type-dry-label').height());
            //Sale Product Label
            label.productOnMobile(".skin-type-oily-label",pos6,height_img,$('.skin-type-oily-label').height());
            //Sale Product Label
            label.productOnMobile(".skin-type-combination-label",pos7,height_img,$('.skin-type-combination-label').height());
            //Sale Product Label
            label.productOnMobile(".skin-type-sensitive-label",pos8,height_img,$('.skin-type-sensitive-label').height());
            //Sale Product Label
            label.productOnMobile(".skin-type-aged-label",pos9,height_img,$('.skin-type-aged-label').height());
            //Sale Product Label
            label.productOnMobile(".skin-type-acne-prone-label",pos10,height_img,$('.skin-type-acne-prone-label').height());

        }
        else{
            //Out of stock label
            label.productOnWindow(".outofstock-label",pos1,width_img-$('.outofstock-label').width()-50,height_img,$('.outofstock-label').height()+20);
            //New Product Label
            label.productOnWindow(".newproduct-label",pos2,width_img-$('.newproduct-label').width()-50,height_img,$('.newproduct-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".saleproduct-label",pos3,width_img-$('.saleproduct-label').width()-50,height_img,$('.saleproduct-label').height()+20);

            //Sale Product Label
            label.productOnWindow(".skin-type-normal-label",pos4,width_img-$('.skin-type-normal-label').width()-50,height_img,$('.skin-type-normal-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".skin-type-dry-label",pos5,width_img-$('.skin-type-dry-label').width()-50,height_img,$('.skin-type-dry-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".skin-type-oily-label",pos6,width_img-$('.skin-type-oily-label').width()-50,height_img,$('.skin-type-oily-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".skin-type-combination-label",pos7,width_img-$('.skin-type-combination-label').width()-50,height_img,$('.skin-type-combination-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".skin-type-sensitive-label",pos8,width_img-$('.skin-type-sensitive-label').width()-50,height_img,$('.skin-type-sensitive-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".skin-type-aged-label",pos9,width_img-$('.skin-type-aged-label').width()-50,height_img,$('.skin-type-aged-label').height()+20);
            //Sale Product Label
            label.productOnWindow(".skin-type-acne-prone-label",pos10,width_img-$('.skin-type-acne-prone-label').width()-50,height_img,$('.skin-type-acne-prone-label').height()+20);

        }
    });

    $('.list-mode').last().remove();
});
