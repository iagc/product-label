/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * =================================================================
 *
 * MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * IAGC Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * IAGC Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
require([
    'jquery',
    'IAGC_ProductLabel/js/label'
], function ($,label) {

    $('.table-comparison .cell').css('position','relative');

    $('.outofstock-list').width('72px');
    $('.newproduct-list').width('72px');
    $('.saleproduct-list').width('72px');

    $('.skin-type-normal-label').width('50px');
    $('.skin-type-dry-label').width('50px');
    $('.skin-type-oily-label').width('50px');
    $('.skin-type-combination-label').width('50px');

    $('.skin-type-sensitive-label').width('50px');
    $('.skin-type-aged-label').width('50px');
    $('.skin-type-acne-prone-label').width('50px');

    $('.product-item-info').css('position','relative');

    var pos1,pos2,pos3,pos4,pos5,pos6,pos7,pos8,pos9,pos10;

    var mode=$('#product-list-mode').html();
    if($('#compare-page').html()>0) {
        $('.outofstock-list').width('72px');
        $('.newproduct-list').width('72px');
        $('.saleproduct-list').width('72px');

        $('.skin-type-normal-label').width('50px');
        $('.skin-type-dry-label').width('50px');
        $('.skin-type-oily-label').width('50px');
        $('.skin-type-combination-label').width('50px');

        $('.skin-type-sensitive-label').width('50px');
        $('.skin-type-aged-label').width('50px');
        $('.skin-type-acne-prone-label').width('50px');

    }
    if(mode=='list')
    {
        var width = $(window).width();
        //Out Of Stock Label
        if(width<600) {
            $('.outofstock-list').width('38px');
            $('.newproduct-list').width('38px');
            $('.saleproduct-list').width('38px');

            $('.skin-type-normal-label').width('38px');
            $('.skin-type-dry-label').width('38px');
            $('.skin-type-oily-label').width('38px');
            $('.skin-type-combination-label').width('38px');

            $('.skin-type-sensitive-label').width('38px');
            $('.skin-type-aged-label').width('38px');
            $('.skin-type-acne-prone-label').width('38px');

        }
        else
        {
            $('.outofstock-list').width('80px');
            $('.newproduct-list').width('80px');
            $('.saleproduct-list').width('80px');

            $('.skin-type-normal-label').width('80px');
            $('.skin-type-dry-label').width('80px');
            $('.skin-type-oily-label').width('80px');
            $('.skin-type-combination-label').width('80px');

            $('.skin-type-sensitive-label').width('80px');
            $('.skin-type-aged-label').width('80px');
            $('.skin-type-acne-prone-label').width('80px');
        }
        pos1 = $('#outofstock-position').html();
        label.onProductList(".outofstock-list", pos1,$('.product-image-wrapper').height());
        //New Product Label
        pos2 = $('#newproduct-position').html();
        label.onProductList(".newproduct-list", pos2,$('.product-image-wrapper').height());
        //Sale Product Label
        pos3 = $('#saleproduct-position').html();
        label.onProductList(".saleproduct-list", pos3,$('.product-image-wrapper').height());

        //Sale Product Label
        pos4 = $('#skin-type-normal-position').html();
        label.onProductList(".skin-type-normal-list", pos4,$('.product-image-wrapper').height());
        //Sale Product Label
        pos5 = $('#skin-type-dry-position').html();
        label.onProductList(".skin-type-dry-list", pos5,$('.product-image-wrapper').height());
        //Sale Product Label
        pos6 = $('#skin-type-oily-position').html();
        label.onProductList(".skin-type-oily-list", pos6,$('.product-image-wrapper').height());
        //Sale Product Label
        pos7 = $('#skin-type-combination-position').html();
        label.onProductList(".skin-type-combination-list", pos7,$('.product-image-wrapper').height());
        //Sale Product Label
        pos8 = $('#skin-type-sensitive-position').html();
        label.onProductList(".skin-type-sensitive-list", pos8,$('.product-image-wrapper').height());
        //Sale Product Label
        pos9 = $('#skin-type-aged-position').html();
        label.onProductList(".skin-type-aged-list", pos9,$('.product-image-wrapper').height());
        //Sale Product Label
        pos10 = $('#skin-type-acne-prone-position').html();
        label.onProductList(".skin-type-acne-prone-list", pos10,$('.product-image-wrapper').height());

        $(window).resize(function(){
            width = $(window).width();
            if(width<600) {
                $('.outofstock-list').width('38px');
                $('.newproduct-list').width('38px');
                $('.saleproduct-list').width('38px');

                $('.skin-type-normal-label').width('38px');
                $('.skin-type-dry-label').width('38px');
                $('.skin-type-oily-label').width('38px');
                $('.skin-type-combination-label').width('38px');

                $('.skin-type-sensitive-label').width('38px');
                $('.skin-type-aged-label').width('38px');
                $('.skin-type-acne-prone-label').width('38px');
            }
            else
            {
                $('.outofstock-list').width('80px');
                $('.newproduct-list').width('80px');
                $('.saleproduct-list').width('80px');

                $('.skin-type-normal-label').width('80px');
                $('.skin-type-dry-label').width('80px');
                $('.skin-type-oily-label').width('80px');
                $('.skin-type-combination-label').width('80px');

                $('.skin-type-sensitive-label').width('80px');
                $('.skin-type-aged-label').width('80px');
                $('.skin-type-acne-prone-label').width('80px');
            }
            //Out Of Stock Label
            pos1 = $('#outofstock-position').html();
            label.onProductList(".outofstock-list", pos1,$('.product-image-wrapper').height());
            //New Product Label
            pos2 = $('#newproduct-position').html();
            label.onProductList(".newproduct-list", pos2,$('.product-image-wrapper').height());
            //Sale Product Label
            pos3 = $('#saleproduct-position').html();
            label.onProductList(".saleproduct-list", pos3,$('.product-image-wrapper').height());

            //Sale Product Label
            pos4 = $('#skin-type-normal-position').html();
            label.onProductList(".skin-type-normal-list", pos4,$('.product-image-wrapper').height());
            //Sale Product Label
            pos5 = $('#skin-type-dry-position').html();
            label.onProductList(".skin-type-dry-list", pos5,$('.product-image-wrapper').height());
            //Sale Product Label
            pos6 = $('#skin-type-oily-position').html();
            label.onProductList(".skin-type-oily-list", pos6,$('.product-image-wrapper').height());
            //Sale Product Label
            pos7 = $('#skin-type-combination-position').html();
            label.onProductList(".skin-type-combination-list", pos7,$('.product-image-wrapper').height());
            //Sale Product Label
            pos8 = $('#skin-type-sensitive-position').html();
            label.onProductList(".skin-type-sensitive-list", pos8,$('.product-image-wrapper').height());
            //Sale Product Label
            pos9 = $('#skin-type-aged-position').html();
            label.onProductList(".skin-type-aged-list", pos9,$('.product-image-wrapper').height());
            //Sale Product Label
            pos10 = $('#skin-type-acne-prone-position').html();
            label.onProductList(".skin-type-acne-prone-list", pos10,$('.product-image-wrapper').height());

        });
    }
    else {
        var width=$('.product-image-photo').width();
        //Out Of Stock Label
        pos1 = $('#outofstock-position').html();
        label.onProductGrid(".outofstock-list", pos1);
        //New Product Label
        pos2 = $('#newproduct-position').html();
        label.onProductGrid(".newproduct-list", pos2);
        //Sale Product Label
        pos3 = $('#saleproduct-position').html();
        label.onProductGrid(".saleproduct-list", pos3);

        //Sale Product Label
        pos4 = $('#skin-type-normal-position').html();
        label.onProductGrid(".skin-type-normal-list", pos4);

        //Sale Product Label
        pos5 = $('#skin-type-dry-position').html();
        label.onProductGrid(".skin-type-dry-list", pos5);

        //Sale Product Label
        pos6 = $('#skin-type-oily-position').html();
        label.onProductGrid(".skin-type-oily-list", pos6);

        //Sale Product Label
        pos7 = $('#skin-type-combination-position').html();
        label.onProductGrid(".skin-type-combination-list", pos7);

        //Sale Product Label
        pos8 = $('#skin-type-sensitive-position').html();
        label.onProductGrid(".skin-type-sensitive-list", pos8);

        //Sale Product Label
        pos9 = $('#skin-type-aged-position').html();
        label.onProductGrid(".skin-type-aged-list", pos9);

        //Sale Product Label
        pos10 = $('#skin-type-acne-prone-position').html();
        label.onProductGrid(".skin-type-acne-prone-list", pos10);

        $(window).resize(function(){

            width=$('.product-image-photo').width();
            //Out Of Stock Label
            pos1 = $('#outofstock-position').html();
            label.onProductGrid(".outofstock-list", pos1,width-$('.outofstock-list').width());
            //New Product Label
            pos2 = $('#newproduct-position').html();
            label.onProductGrid(".newproduct-list", pos2,width-$('.newproduct-list').width());
            //Sale Product Label
            pos3 = $('#saleproduct-position').html();
            label.onProductGrid(".saleproduct-list", pos3,width-$('.saleproduct-list').width());

            //New Product Label
            pos4 = $('#skin-type-normal-position').html();
            label.onProductGrid(".skin-type-normal-list", pos4,width-$('.skin-type-normal-list').width());

            //New Product Label
            pos5 = $('#skin-type-dry-position').html();
            label.onProductGrid(".skin-type-dry-list", pos5,width-$('.skin-type-dry-list').width());

            //New Product Label
            pos6 = $('#skin-type-oily-position').html();
            label.onProductGrid(".skin-type-oily-list", pos6,width-$('.skin-type-oily-list').width());

            //New Product Label
            pos7 = $('#skin-type-combination-position').html();
            label.onProductGrid(".skin-type-combination-list", pos7,width-$('.skin-type-combination-list').width());

            //New Product Label
            pos8 = $('#skin-type-sensitive-position').html();
            label.onProductGrid(".skin-type-sensitive-list", pos8,width-$('.skin-type-sensitive-list').width());

            //New Product Label
            pos9 = $('#skin-type-aged-position').html();
            label.onProductGrid(".skin-type-aged-list", pos9,width-$('.skin-type-aged-list').width());

            //New Product Label
            pos10 = $('#skin-type-acne-prone-position').html();
            label.onProductGrid(".skin-type-acne-prone-list", pos10,width-$('.skin-type-acne-prone-list').width());


        });
    }
});
