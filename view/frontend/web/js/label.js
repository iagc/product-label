/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * =================================================================
 *
 * MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This package designed for Magento COMMUNITY edition
 * IAGC Commerce does not guarantee correct work of this extension
 * on any other Magento edition except Magento COMMUNITY edition.
 * IAGC Commerce does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2015-2016 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
define([
    "jquery",
    "jquery/ui"
], function($) {
    "use strict";
    var label = {};

    //display label on product page on mobile
    label.productOnMobile = function(className,pos,height_img,height_label)
    {
        $('.outofstock-label').width('50px');
        $('.newproduct-label').width('50px');
        $('.saleproduct-label').width('50px');

        $('.skin-type-normal-label').width('50px');
        $('.skin-type-dry-label').width('50px');
        $('.skin-type-oily-label').width('50px');
        $('.skin-type-combination-label').width('50px');

        $('.skin-type-sensitive-label').width('50px');
        $('.skin-type-aged-label').width('50px');
        $('.skin-type-acne-prone-label').width('50px');

        $(className).css('position','absolute');
        $(className).css('z-index','1');

        switch (pos){
/*RIGHT*/
            case "right-top":
                $(className).css('right',"10px");
                $(className).css('top',"10px");
                $(className).css('left',"none");
                break;
            case "center-top":
                $(className).css('right',"10px");
                $(className).css('top',"10px");
                $(className).css('left',"none");
                break;
            case "left-top":
                $(className).css('left','10px');
                $(className).css('top',"10px");
                break;
/*CENTER*/
            case "right-middle":
                $(className).css('right',"10px");
                $(className).css('top',height_img-height_label-10);
                $(className).css('left',"none");
                break;
            case "center-middle":
                $(className).css('right',"10px");
                $(className).css('top',height_img-height_label-10);
                $(className).css('left',"none");
                break;
            case "left-middle":
                $(className).css('left','10px');
                $(className).css('top',height_img-height_label-10);
                break;

/*LEFT*/
            case "right-bottom":
                $(className).css('right',"10px");
                $(className).css('top',height_img-height_label-10);
                $(className).css('left',"none");
                break;
            case "center-bottom":
                $(className).css('right',"10px");
                $(className).css('top',height_img-height_label-10);
                $(className).css('left',"none");
                break;
            case "left-bottom":
                $(className).css('left','10px');
                $(className).css('top',height_img-height_label-10);
                break;
        }
    }

    //display label on product page on window
    label.productOnWindow=function (className,pos,width,height,height_label) {
        $('.outofstock-label').width('100px');
        $('.newproduct-label').width('100px');
        $('.saleproduct-label').width('100px');

        $('.skin-type-normal-label').width('100px');
        $('.skin-type-dry-label').width('100px');
        $('.skin-type-oily-label').width('100px');
        $('.skin-type-combination-label').width('100px');

        $('.skin-type-sensitive-label').width('100px');
        $('.skin-type-aged-label').width('100px');
        $('.skin-type-acne-prone-label').width('100px');

        $(className).css('position','absolute');
        $(className).css('z-index','1');
        switch (pos){

            case "right-top":
                $(className).css('left',width-10);
                $(className).css('top','20px');
                $(className).css('right',"none");
                break;
            case "center-top":
                $(className).css('left',width-10);
                $(className).css('top','20px');
                $(className).css('right',"none");
                break;
            case "left-top":
                $(className).css('left','0px');
                $(className).css('top','20px');
                break;

            case "right-middle":
                $(className).css('left',width-10);
                $(className).css('top',height-height_label*2);
                $(className).css('right',"none");
                break;
            case "center-middle":
                $(className).css('left',width-10);
                $(className).css('top',height-height_label*2);
                $(className).css('right',"none");
                break;
            case "left-middle":
                $(className).css('left',"50px");
                $(className).css('top',height-height_label*2);
                break;

            case "right-bottom":
                $(className).css('left',width-10);
                $(className).css('top',height-height_label*2);
                $(className).css('right',"none");
                break;
            case "center-bottom":
                $(className).css('left',width-10);
                $(className).css('top',height-height_label*2);
                $(className).css('right',"none");
                break;
            case "left-bottom":
                $(className).css('left',"50px");
                $(className).css('top',height-height_label*2);
                break;

        }
    }

    //display label on product list page grid mode
    label.onProductGrid=function(className,pos){
        $(className).css('position','absolute');
        $(className).css('z-index','1');
        switch (pos){

            case "right-top":
                $(className).css('right','0px');
                $(className).css('top','0px');
                break;
            case "center-top":
                $(className).css('right','90px');
                $(className).css('top','0px');
                break;
            case "left-top":
                $(className).css('left','0px');
                $(className).css('top','0px');
                break;

            case "right-middle":
                $(className).css('right','0px');
                $(className).css('margin-top','-95px');
                break;
            case "center-middle":
                $(className).css('right','37px');
                $(className).css('margin-top','-95px');
                break;
            case "left-middle":
                $(className).css('left','0px');
                $(className).css('margin-top','-95px');
                break;

            case "right-bottom":
                $(className).css('right','0px');
                $(className).css('margin-top','-60px');
                break;
            case "center-bottom":
                $(className).css('right','37px');
                $(className).css('margin-top','-60px');
                break;
            case "left-bottom":
                $(className).css('right','70px');
                $(className).css('margin-top','-60px');
                break;

        }
    }

    //display label on product list page list mode
    label.onProductList=function(className,pos,height){
        $('.list-mode').css('position','relative')
        $(className).css('position','absolute');
        $(className).css('z-index','1');
        switch (pos){
            case "right-top":
                $(className).css('right','0px');
                $(className).css('top',-height);
                break;
            case "center-top":
                $(className).css('right','0px');
                $(className).css('top',-height);
                break;
            case "left-top":
                $(className).css('left','0px');
                $(className).css('top',-height);
                break;

            case "right-middle":
                $(className).css('right','0px');
                $(className).css('margin-top','-60px');
                break;
            case "center-middle":
                $(className).css('right','0px');
                $(className).css('margin-top','-60px');
                break;
            case "left-middle":
                $(className).css('left','0px');
                $(className).css('margin-top','-60px');
                break;

            case "right-bottom":
                $(className).css('right','0px');
                $(className).css('margin-top','-60px');
                break;
            case "center-bottom":
                $(className).css('right','0px');
                $(className).css('margin-top','-60px');
                break;
            case "left-bottom":
                $(className).css('left','0px');
                $(className).css('margin-top','-60px');
                break;

        }
    }

    return label;
});
