<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Model\Attribute\Source;

class Label extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource
{
    /**
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('Use General Config'), 'value' => 'none'],
                ['label' => __('Label Type 1'), 'value' => 'new'],
                ['label' => __('Label Type 2'), 'value' => 'sale'],
                ['label' => __('Normal Skin'), 'value' => 'normal'],
                ['label' => __('Dry Skin'), 'value' => 'dry'],
                ['label' => __('Oily Skin'), 'value' => 'oily'],
                ['label' => __('Combination Skin'), 'value' => 'combination'],
                ['label' => __('Sensitive Skin'), 'value' => 'sensitive'],
                ['label' => __('Aged Skin'), 'value' => 'aged'],
                ['label' => __('Acne Prone Skin'), 'value' => 'acne_prone']
            ];
        }
        return $this->_options;
    }
}
