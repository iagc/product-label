<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Model\Config\Source;

class Position implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'right-top', 'label' => __('Top Right')],
            ['value' => 'center-top', 'label' => __('Top Center')],
            ['value' => 'left-top', 'label' => __('Top Left')],

            ['value' => 'right-middle', 'label' => __('Middle Right')],
            ['value' => 'center-middle', 'label' => __('Middle Center')],
            ['value' => 'left-middle', 'label' => __('Middle Left')],

            ['value' => 'right-bottom', 'label' => __('Bottom Right')],
            ['value' => 'center-bottom', 'label' => __('Bottom Center')],
            ['value' => 'left-bottom', 'label' => __('Bottom Left')]
        ];

    }
}
