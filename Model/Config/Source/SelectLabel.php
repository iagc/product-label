<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Model\Config\Source;

class SelectLabel implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'outofstock', 'label' => __('Out Of Stock')],
            ['value' => 'new', 'label' => __('New')],
            ['value' => 'sale', 'label' => __('Sale')],
            ['value' => 'normal', 'label' => __('Normal')],
            ['value' => 'dry', 'label' => __('Dry')],
            ['value' => 'oily', 'label' => __('Oily')],
            ['value' => 'combination', 'label' => __('Combination')],
            ['value' => 'sensitive', 'label' => __('Sensitive')],
            ['value' => 'aged', 'label' => __('Aged')],
            ['value' => 'acne_prone', 'label' => __('Acne Prone')]
        ];
    }
}
