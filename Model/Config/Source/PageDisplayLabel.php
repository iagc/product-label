<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Model\Config\Source;

class PageDisplayLabel
{
    /**
     * @return array
     */
    public static function toOptionArray()
    {
        return [
            ['value' => 'productpage', 'label'=>__('Product Page')],
            ['value' => 'productlist', 'label'=>__('Product List Page')]
        ];
    }
}
