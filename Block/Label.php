<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Block;

use Magento\Framework\View\Element\Template;
use Magento\CatalogInventory\Api\StockRegistryInterface;

class Label extends Template
{
    /**
     * @var \IAGC\ProductLabel\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var \Magento\Framework\View\LayoutInterface
     */
    protected $layout;

    /**
     * @var StockRegistryInterface
     */
    protected $stockRegistry;

    /**
     * Label constructor.
     * @param Template\Context $context
     * @param \IAGC\ProductLabel\Helper\Data $helper
     * @param \Magento\Framework\Registry $registry
     * @param StockRegistryInterface $stockRegistry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \IAGC\ProductLabel\Helper\Data $helper,
        \Magento\Framework\Registry $registry,
        StockRegistryInterface $stockRegistry,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper=$helper;
        $this->stockRegistry = $stockRegistry;
        $this->registry = $registry;
        $this->layout = $context->getLayout();
    }

    /**
     * @return mixed
     */
    public function getProductListBlock() {
        $html = $this->layout
            ->createBlock('IAGC\ProductLabel\Block\Label')
            ->setTemplate('IAGC_ProductLabel::productlist.phtml')
            ->toHtml();
        return $html;
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return \Magento\CatalogInventory\Api\Data\StockItemInterface
     */
    public function getStockItem($product)
    {
        $stockItem = $this->stockRegistry->getStockItem($product->getId(), $product->getStore()->getWebsiteId());
        return $stockItem;

    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function getProduct(){
        return $this->registry->registry('current_label_product');
    }

    /**
     * @param StockRegistryInterface $productStock
     * @param string $class
     * @return string
     */
    public function getOutOfStockLabel($productStock,$class)
    {
        if ($this->helper->isEnableOutOfStockLabel()) {
            // echo '<pre>'; var_dump($this->helper->isEnableOutOfStockLabel()); echo '</pre>';
            $imgName = $this->helper->getOutOfStockImageName();
            $src = $this->helper->resize($imgName);
            // if ($productStock->getIsInStock() == 0) {
                return "<div class='list-mode'><img class='$class' src='$src'></div>";
            // }
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getNewProductLabel($product, $class)
    {
        if ($this->helper->isEnableNewProductLabel()) {
            $imgName = $this->helper->getNewProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSaleProductLabel($product, $class)
    {
        if ($this->helper->isEnableSaleProductLabel()) {
            $imgName = $this->helper->getSaleProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }
/* ---- -------------------------- */
    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeNormalProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeNormalLabel()) {
            $imgName = $this->helper->getSkinTypeNormalProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeDryProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeDryLabel()) {
            $imgName = $this->helper->getSkinTypeDryProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeOilyProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeOilyLabel()) {
            $imgName = $this->helper->getSkinTypeOilyProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeCombinationProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeCombinationLabel()) {
            $imgName = $this->helper->getSkinTypeCombinationProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeSensitiveProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeSensitiveLabel()) {
            $imgName = $this->helper->getSkinTypeSensitiveProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeAgedProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeAgedLabel()) {
            $imgName = $this->helper->getSkinTypeAgedProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param string $class
     * @return string
     */
    public function getSkinTypeAcneProneProductLabel($product, $class)
    {
        if ($this->helper->isEnableSkinTypeAcneProneLabel()) {
            $imgName = $this->helper->getSkinTypeAcneProneProductImageName();
            $src = $this->helper->resize($imgName);
            return "<div class='list-mode'><img class='$class' src='$src'></div>";
        }
        return "";
    }    
    
/* ---- -------------------------- */

    /**
     * @return string
     */
    public function getOutOfStockPosition()
    {
        return $this->helper->getOutOfStockPosition();
    }

    /**
     * @return string
     */
    public function getNewProductPosition()
    {
        return $this->helper->getNewProductPosition();
    }

    /**
     * @return string
     */
    public function getSaleProductPosition()
    {
        return $this->helper->getSaleProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeNormalProductPosition()
    {
        return $this->helper->getSkinTypeNormalProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeDryProductPosition()
    {
        return $this->helper->getSkinTypeDryProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeOilyProductPosition()
    {
        return $this->helper->getSkinTypeOilyProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeCombinationProductPosition()
    {
        return $this->helper->getSkinTypeCombinationProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeSensitiveProductPosition()
    {
        return $this->helper->getSkinTypeSensitiveProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeAgedProductPosition()
    {
        return $this->helper->getSkinTypeAgedProductPosition();
    }

    /**
     * @return string
     */
    public function getSkinTypeAcneProneProductPosition()
    {
        return $this->helper->getSkinTypeAcneProneProductPosition();
    }

        /* ---- -------------------------- */

}
