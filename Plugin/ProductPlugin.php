<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Plugin;

use IAGC\ProductLabel\Block\Label;

class ProductPlugin
{

    /**
     * @var Label
     */
    protected $label;

    /**
     * @var \IAGC\ProductLabel\Helper\data
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * ProductPlugin constructor.
     * @param Label $label
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \IAGC\ProductLabel\Helper\data $helper
     */
    public function __construct(
        Label $label,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \IAGC\ProductLabel\Helper\Data $helper
    ) {
        $this->label=$label;
        $this->helper=$helper;
        $this->productFactory = $productFactory;
    }

    /**
     * @param \Magento\Catalog\Block\Product\View\Gallery $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(\Magento\Catalog\Block\Product\View\Gallery $subject, $result)
    {
        $product = $subject->getProduct();

        // $parentProduct = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($productId);
        //
        // echo '<pre>'; var_dump($product->getSku()); echo '</pre>';
        // echo '<pre>'; var_dump($product[0]->getSku()); echo '</pre>'; die();

        // if ($product->getTypeId() == 'configurable') {
        //     $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);
        //     foreach ($_children as $child){
        //         echo "Here are your child Product Ids ".$child->getID()."\n";
        //     }
        //     echo "count: ".count($_children);
        // }

        if ($product != null) {

            $productStock = $this->label->getStockItem($product);
            $pos = strpos($this->helper->isDisplayOn(), "productpage");

            if (($this->helper->isEnable() == true) && (is_int($pos))) {

                /**
                 * New Products
                 */
                if ($product->getTypeId() == 'configurable') {
                    $productId = $product->getId();
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $configProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);

                    $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);

                    foreach ($_children as $child){
                        if ($this->isNewItem($child)) {
                            $result .= $this->label->getNewProductLabel($product, "newproduct-label");
                            break;
                        }
                    }
                } 
                else 
                {
                    if ($this->isNewItem($product)) {
                        $result .= $this->label->getNewProductLabel($product, "newproduct-label");
                    }
                }


                /**
                 * Best Seller Products
                 */
                if ($product->getTypeId() == 'configurable') {
                    $productId = $product->getId();
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $configProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);

                    $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);

                    foreach ($_children as $child){
                        if ($this->isBestSellerItem($child)) {
                            $result .= $this->label->getSaleProductLabel($product, "saleproduct-label");
                            break;
                        }
                    }
                } 
                else 
                {
                    if ($this->isBestSellerItem($product)) {
                        $result .= $this->label->getSaleProductLabel($product, "saleproduct-label");
                    }
                }

                /**
                 * Sale Products
                 */

                // echo '<pre>'; var_dump($product->getTypeId()); echo '</pre>'; die();
                if ($product->getTypeId() == 'configurable') {
                    $productId = $product->getId();
                    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                    $configProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);

                    $_children = $configProduct->getTypeInstance()->getUsedProducts($configProduct);

                    foreach ($_children as $child){
                        if ($this->isSaleItem($child)) {
                            $result .= $this->label->getOutOfStockLabel($product, "outofstock-label");
                            break;
                        }
                    }
                } 
                else 
                {
                    if ($this->isSaleItem($product)) {
                        $result .= $this->label->getOutOfStockLabel($product, "outofstock-label");
                    }
                }
            }
        }

        return $result;
    }

    public function isNewItem($product) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $collection1 = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\Collection'); 
        $collection1->addAttributeToSelect('*')
            ->addAttributeToFilter(
                'news_from_date',
                ['date' => true, 'from' => date('Y-m-01 00:00:00', strtotime('-1 month'))],
                'left')
                ->addAttributeToSort('news_from_date', 'desc')
                ->addFieldToFilter('sku', array('like' => $product->getSku() .'%'))
                ->addStoreFilter($this->helper->getStoreId())
                ->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
                ->setCurPage(1);

        $r1 = [];

        if ($collection1->getSize() > 0)
        {
            foreach ($collection1 as $item) {
                $r1[] = $item->getId();
            }

            if (count($r1) > 0) {
                return true;
            }

        }

        return false;

    }

    public function isBestSellerItem($product) {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCollection = $objectManager->create('Magento\Reports\Model\ResourceModel\Report\Collection\Factory'); 
        $collection = $productCollection->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection'); 

        $collection->setPeriod('month');
        foreach ($collection as $item) {
            $r[] = $item->getProductId();
        }

        if (in_array($product->getId(), $r)) {
            return true;
        }

        return false;

    }

    public function isSaleItem($product) {

        $specialPrice = $product->getSpecialPrice();
        $price = $product->getPrice();

        $fromDate = $product->getSpecialFromDate();
        $toDate = $product->getSpecialToDate();

        $fromDate = strtotime($fromDate);
        $toDate = strtotime($toDate);
        $now = strtotime('now');

        if (!is_null($fromDate)) {
            if (!is_null($toDate)) {
                if ($fromDate <= $now && $toDate >= $now) {
                    if (!is_null($specialPrice)) {
                        return true;
                    }
                }
            }
        }

        return false;

    }


}
