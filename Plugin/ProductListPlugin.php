<?php
/**
 * IAGC Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://iagc.com/IAGC-Commerce-License.txt
 *
 * @category   IAGC
 * @package    IAGC_ProductLabel
 * @author     Extension Team
 * @copyright  Copyright (c) 2017-2018 IAGC Commerce Co. ( http://iagc.com )
 * @license    http://iagc.com/IAGC-Commerce-License.txt
 */
namespace IAGC\ProductLabel\Plugin;

use IAGC\ProductLabel\Block\Label;

class ProductListPlugin
{
    /**
     * @var Label
     */
    protected $label;

    /**
     * @var \IAGC\ProductLabel\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    private $ctrNew;
    /**
     * ProductListPlugin constructor.
     * @param Label $label
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \IAGC\ProductLabel\Helper\Data $helper
       @param \Magento\Sales\Model\ResourceModel\Report\Bestsellers\CollectionFactory $bestSellerCollectionFactory
     */
    public function __construct(
        Label $label,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \IAGC\ProductLabel\Helper\Data $helper
    ) {
        $this->label=$label;
        $this->productFactory = $productFactory;
        $this->helper=$helper;
        $this->ctrNew = 0;
        // echo '<pre>'; var_dump($this->ctrNew); echo '</pre>'; die();
    }

    /**
     * @param \Magento\Catalog\Block\Product\Image $subject
     * @param $result
     * @return string
     */
    public function afterToHtml(\Magento\Catalog\Block\Product\Image $subject, $result)
    {
        $result.=$this->label->getProductListBlock();
        $product = $this->label->getProduct();

        // echo '<pre>'; var_dump('adrian'); echo '</pre>'; die();
        if ($product != null) {

            $productStock = $this->label->getStockItem($product);
            $selectedLabel = $product->getResource()->getAttributeRawValue(
                $product->getId(),
                'select_label',
                $this->helper->getStoreId()
            );

            $pos = strpos($this->helper->isDisplayOn(), "productlist");

            if (($this->helper->isEnable() == true) && (is_int($pos))) {

                /**
                 * New Products
                 */

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $collection1 = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\Collection'); 
                $collection1->addAttributeToSelect('*')
                    ->addAttributeToFilter(
                        'news_from_date',
                        ['date' => true, 'from' => date('Y-m-01 00:00:00', strtotime('-1 month'))],
                        'left')
                    ->addAttributeToSort('news_from_date', 'desc')
                    ->addFieldToFilter('sku', array('like' => $product->getSku() .'%'))
                    ->addStoreFilter($this->helper->getStoreId())
                    ->addAttributeToFilter('type_id', \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE)
                    ->setCurPage(1);

                $r1 = [];

                if ($collection1->getSize() > 0)
                {
                    foreach ($collection1 as $item) {
                        $r1[] = $item->getId();
                    }

                    $maxNew = 15;
                    if ($this->ctrNew < $maxNew) {
                        if (count($r1) > 0) {
                            $result .= $this->label->getNewProductLabel($product, "newproduct-list");
                        }
                        $this->ctrNew++;
                    } 

                }

                /**
                 * Best Seller Products
                 */

                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $productCollection = $objectManager->create('Magento\Reports\Model\ResourceModel\Report\Collection\Factory'); 
                $collection = $productCollection->create('Magento\Sales\Model\ResourceModel\Report\Bestsellers\Collection'); 

                $collection->setPeriod('month');
                foreach ($collection as $item) {
                    $r[] = $item->getProductId();
                }

                if (in_array($product->getId(), $r)) {
                    $result .= $this->label->getSaleProductLabel($product, "saleproduct-list");
                }

                /**
                 * Sale Products
                 */
                $specialPrice = $product->getSpecialPrice();
                $price = $product->getPrice();

                $fromDate = $product->getSpecialFromDate();
                $toDate = $product->getSpecialToDate();

                // echo '<pre>'; var_dump($fromDate, $toDate, date('Y-m-d') ); echo '</pre>'; die();
                $fromDate = strtotime($fromDate);
                $toDate = strtotime($toDate);
                $now = strtotime('now');

                if (!is_null($fromDate)) {
                    if (!is_null($toDate)) {
                        if ($fromDate <= $now && $toDate >= $now) {
                            if (!is_null($specialPrice)) {
                                $result .= $this->label->getOutOfStockLabel($product, "outofstock-list");
                            }
                        }
                    }
                }

            }

            $res = $product->getResource()->getAttributeRawValue(
                $product->getId(),
                'skin_type_product',
                $this->helper->getStoreId()
            );

            $res = explode(",", $res);

            for ($i = 0; $i < count($res); $i++) {
                $attr = $product->getResource()->getAttribute('skin_type_product');
                if ($attr->usesSource()) {
                    $selectedSkinType[] = $attr->getSource()->getOptionText($res[$i]);
                }
            }

            if (($this->helper->isEnable() == true) && (is_int($pos))) {

                $selectedSkinType = array_map('strtolower', $selectedSkinType);

                //selected use general config
                if ((in_array("normal", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeNormalProductLabel($product, "skin-type-normal-list");
                }

                //selected use general config
                if ((in_array("dry", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeDryProductLabel($product, "skin-type-dry-list");
                }

                //selected use general config
                if ((in_array("oily", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeOilyProductLabel($product, "skin-type-oily-list");
                }

                //selected use general config
                if ((in_array("combination", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeCombinationProductLabel($product, "skin-type-combination-list");
                }

                //selected use general config
                if ((in_array("sensitive", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeSensitiveProductLabel($product, "skin-type-sensitive-list");
                }

                //selected use general config
                if ((in_array("aged", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeAgedProductLabel($product, "skin-type-aged-list");
                }

                //selected use general config
                if ((in_array("acne prone", $selectedSkinType))) {
                    $result .= $this->label->getSkinTypeAcneProneProductLabel($product, "skin-type-acne-prone-list");
                }
            }
        }
        return $result;
    }

}
